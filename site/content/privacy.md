# Privacy Policy

The deviceneutrality.org web site is run by the [FSFE e.V.](https://fsfe.org) When this policy refers to “we”, this means the FSFE.

## What we collect and why

When you use the website, we collect and store information about the visit this includes your IP address, the browser you use and the time of your visit. These information are needed to serve the content to you. They are stored automatically by the server in log files and deleted after 7 days automatically.

## Who has access to stored data

All information submitted through the website, whether stored on the web server or in our issue system, is available to FSFE staff and interns. The information is also available to FSFE’s system administrator team. Information is stored unencrypted on our servers, which means the information is also available to staff of noris network AG where our servers are hosted.

## What your rights are

You have the right to receive the personal data we have stored about you, as well as the right to instruct us to rectify it if it’s incorrect. You also have the right to object to our storing of information, in which case we will remove personal data about you, and the right to remove your consent to processing of your personal data in line with this policy, in which case we will also take steps to remove personal data about you.

## Who to contact

If you have questions about our use of your data, would like to request a copy of all information we have stored about you, please contact [privacy@fsfe.org](mailto:privacy@fsfe.org).
